class CreateSurvivors < ActiveRecord::Migration[5.1]
  def change
    create_table :survivors do |t|
      t.string :name
      t.integer :age, default: 0
      t.string :gender
      t.float :latitude
      t.float :longitude
      t.boolean :infected, default: false
      # t.jsonb :inventory

      t.timestamps
    end
  end
end
