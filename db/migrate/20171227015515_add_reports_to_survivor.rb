class AddReportsToSurvivor < ActiveRecord::Migration[5.1]
  def up
    add_column :survivors, :reports, :text, array: true, default: '{}'
  end

  def down
    remove_column :survivors, :reports
  end
end
