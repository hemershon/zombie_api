FactoryBot.define do
  factory :resource do
    item { ['Water', 'Food', 'Medication', 'Ammunition'].sample }
    survivor
  end
end
