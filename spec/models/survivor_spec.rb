require 'rails_helper'

RSpec.describe Survivor, type: :model do
  let(:survivor) { FactoryBot.create(:survivor, :with_resources) }

  it { is_expected.to have_many :resources }
  it { is_expected.to accept_nested_attributes_for(:resources) }
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :age }
  it { is_expected.to validate_numericality_of(:age) }

  context 'When survivor is reported more than 3 times' do
    describe '#set_infected_when_reported' do
      it 'return false if no infected' do
        expect(survivor.infected).to be_falsey
      end

      it 'returns true if is reported' do
        reports = ['infected', 'infected', 'infected']
        survivor.update(reports: reports)

        expect(survivor.infected).to be_truthy
      end
    end
  end
end
