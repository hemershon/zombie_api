require 'rails_helper'

RSpec.describe Dashboard, type: :model do
  FactoryBot.create_list(:survivor, 10, :with_resources)
  
  describe '.show' do
    it 'returns json/hash with dashboard info' do
      Survivor.last.update(infected: true)
      expect(Dashboard.show).to include("Infected average", 
        "Non infected average",
        "Resources by survivors",
        "Lost points")
    end
  end
end
