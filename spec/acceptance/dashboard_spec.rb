require 'rails_helper'
require "rspec_api_documentation/dsl"


resource "Dashboard" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"

  # GET index
  get 'api/v1/dashboard' do
    example_request 'Return dashboard info' do
      do_request

      expect(status).to eq(200)
    end
  end
end