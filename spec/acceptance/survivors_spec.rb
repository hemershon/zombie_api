require 'rails_helper'
require "rspec_api_documentation/dsl"


resource "Survivors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"

  let(:survivor) { FactoryBot.create(:survivor, :with_resources) }
  let(:other_survivor) { FactoryBot.create(:survivor, :with_resources) }

  # GET index
  get 'api/v1/survivors' do
    example 'Listing all survivors' do
      do_request

      expect(status).to eq(200)
    end
  end

  # GET show
  get 'api/v1/survivors/:id' do
    let(:id) { FactoryBot.create(:survivor).id }
    example 'Get a survivor' do
      do_request

      expect(path).to eq "api/v1/survivors/#{id}"
    end
  end

  # POST create
  post 'api/v1/survivors' do

    survivor = FactoryBot.build(:survivor)
    resources = FactoryBot.build_list(:resource, 3, survivor_id: survivor.id)

    parameter :survivor, survivor
    let(:survivor) { survivor }

    parameter :name, survivor.name
    let(:name) { survivor.name}

    parameter :age, survivor.age
    let(:age) { survivor.age }

    parameter :gender, survivor.gender
    let(:gender) { survivor.gender }

    parameter :latitude, survivor.latitude
    let(:latitude) { survivor.latitude }

    parameter :longitude, survivor.longitude
    let(:longitude) { survivor.longitude }

    parameter :infected, survivor.infected
    let(:infected) { survivor.infected }

    parameter :reports, survivor.reports
    let(:reports) { survivor.infected}

    parameter :resources_attributes, resources.to_json
    let(:resources_attributes) { resources.to_json }

    let(:raw_post) { params.to_json }

    example_request 'Create a survivor' do
      do_request

      expect(status).to eq(201)
    end
  end

  # PUT update
  put 'api/v1/survivors/:id' do

    let(:id) { survivor.id }

    parameter :reports, []
    let(:reports) { ['infected'] }

    let(:raw_post) { params.to_json }

    example_request 'Updating reports' do
      do_request

      expect(status).to eq(200)
    end
  end

  # PUT update location
  put 'api/v1/survivors/:id/update_location' do

    let(:id) { survivor.id }

    parameter :latitude, FFaker::Geolocation.lat
    let(:latitude) { FFaker::Geolocation.lat }

    parameter :longitude, FFaker::Geolocation.lng
    let(:longitude) { FFaker::Geolocation.lng }

    let(:raw_post) { params.to_json }

    example_request 'Updating survivor locale' do
      do_request

      expect(status).to eq(200)
    end
  end

  # PUT trade
  put 'api/v1/survivors/:id/trade' do
    before do
      FactoryBot.create_list(:resource, 2,
        survivor_id: survivor.id,
        item: "Water")

      FactoryBot.create(:resource,
        survivor_id: survivor.id,
        item: "Food")

      FactoryBot.create_list(:resource, 3,
        survivor_id: other_survivor.id,
        item: "Food")

      FactoryBot.create(:resource,
        survivor_id: other_survivor.id,
        item: "Medication")
    end

    resources =
      {
        "items": [
          {
            "item": "Water",
            "quantity": 2
          },
          {
            "item": "Food",
            "quantity": 1
          }
        ]
      }

    let(:id) { survivor.id }

    parameter :item, "Water"
    parameter :quantity, "2"


    let(:params) { resources }

    let(:raw_post) { params.to_json }

    example_request 'Trading resources between survivors' do
      do_request

      message = {
        "message": "Exchanged with success!"
      }

      expect(response_body).to eq(message.to_json)
    end
  end
end
