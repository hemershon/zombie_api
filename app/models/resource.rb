class Resource < ApplicationRecord
  belongs_to :survivor

  ITEMS = ["Water", "Food", "Medication", "Ammunition"].freeze

  validates :item, presence: true, inclusion: { in: ITEMS }
  validate :check_infected_survivor, on: [:create, :update]

  def check_infected_survivor
    if self.survivor.infected
      errors.add(:survivor, 
        "Infected survivor")
    end
  end
end
