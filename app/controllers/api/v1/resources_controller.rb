module Api::V1
  class ResourcesController < ApplicationController
    before_action :set_resource, only: %i[check_infected_survivor show update]
    before_action :check_infected_survivor, except: :create

    # POST /resources
    def create
      @resource = Resource.new(resource_params)

      if @resource.save
        render json: @resource, status: :created
      else
        render json: { errors: @resource.errors }, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /resources/1
    def update
      if @resource.update(resource_params)
        render json: @resource
      else
        render json: { errors: @resource.errors }, status: :unprocessable_entity
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource
      @resource = Resource.find(params[:id])
    end

    def check_infected_survivor
      render json: { errors: "Infected survivor" }, status: 401 if @resource.survivor.infected
    end

    # Only allow a trusted parameter "white list" through.
    def resource_params
      params.require(:resource).permit(:item, :survivor_id)
    end
  end
end